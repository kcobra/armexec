/*
 * This file is part of armexec.
 *
 * armexec is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * armexec is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with armexec.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "armexec.h"
#include "plt_stub.h"

DCLR_STUB(dummy)
{
    return 0;
}

elf_stub_t g_plt_stubs[] = {
    /* stdlib_stub */
    STUB_ENT(malloc),
    STUB_ENT(realloc),
    STUB_ENT(strtoul),
    STUB_ENT(srand48),
    STUB_ENT(bsd_signal),
    STUB_ENT(usleep),

    /* string stub */
    STUB_ENT(memset),
    STUB_ENT(memcpy),
    STUB_ENT(memmove),
    STUB_ENT(strcpy),
    STUB_ENT(strncpy),
    STUB_ENT(strlen),
    STUB_ENT(strdup),
    STUB_ENT(strcmp),
    STUB_ENT(strcasecmp),
    STUB_ENT(strncmp),
    STUB_ENT(strcat),
    STUB_ENT(strncat),
    STUB_ENT(strchr),
    STUB_ENT(memcmp),
    STUB_ENT(strstr),
    STUB_ENT(memchr),
    STUB_ENT(strncasecmp),
    STUB_ENT(strrchr),

    /* sprintf stub */
    STUB_ENT(sprintf),
    STUB_ENT(printf),

    /* socket stub */
    STUB_ENT(inet_addr),
    STUB_ENT(inet_ntoa),
    STUB_ENT(pipe),
    STUB_ENT(close),
    STUB_ENT(socket),
    STUB_ENT(bind),
    STUB_ENT(listen),
    STUB_ENT(write),
    STUB_ENT(select),
    STUB_ENT(read),
    STUB_ENT(sendto),
    STUB_ENT(send),
    STUB_ENT(connect),
    STUB_ENT(recv),
    STUB_ENT(shutdown),
    STUB_ENT(getsockname),
    STUB_ENT(getaddrinfo),
    STUB_ENT(fcntl),
    STUB_ENT(ioctl),
    STUB_ENT(setsockopt),
    STUB_ENT(getsockopt),
    STUB_ENT(recvfrom),
    STUB_ENT(accept),

    /* time stub */
    STUB_ENT(time),
    STUB_ENT(gettimeofday),
    STUB_ENT(nanosleep),
    STUB_ENT(gmtime),

    /* pthread stub */
    STUB_ENT(pthread_create),
    STUB_ENT(pthread_mutex_init),
    STUB_ENT(pthread_mutex_destroy),
    STUB_ENT(pthread_self),
    STUB_ENT(pthread_detach),
    STUB_ENT(pthread_join),
    STUB_ENT(pthread_exit),
    STUB_ENT(pthread_mutex_lock),
    STUB_ENT(pthread_mutex_unlock),
    STUB_ENT(sem_init),
    STUB_ENT(sem_destroy),
    STUB_ENT(sem_wait),
    STUB_ENT(sem_post),
    STUB_ENT(__errno),
    STUB_ENT(sched_yield),
    STUB_ENT(sem_getvalue),
    STUB_ENT(pthread_attr_setdetachstate),

    /* dummy */
    STUB_ENT_DUM(pthread_attr_init),
    STUB_ENT_DUM(free),
    STUB_ENT_DUM(freeaddrinfo),
};

int g_plt_stubs_cnt = sizeof(g_plt_stubs) / sizeof(elf_stub_t);

elf_stub_t g_prog_stubs[] = {
    /* mq */
    STUB_ENT(mq_open),
    STUB_ENT(mq_send),
    STUB_ENT(mq_close),
    STUB_ENT(mq_timedreceive),
};

int g_prog_stubs_cnt = sizeof(g_prog_stubs) / sizeof(elf_stub_t);
