/*
 * This file is part of armexec.
 *
 * armexec is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * armexec is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with armexec.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef _LOG_H
#define _LOG_H

#include <stdio.h>
#include <stdlib.h>

extern __thread uint32_t exec_curr_insn;

#define INFO_ON 1

#define PANIC(fmt, ...) do { \
    fprintf(stderr, "[%d]:[%x]: ", thread_id, exec_curr_insn); \
    fprintf(stderr, fmt, ##__VA_ARGS__); \
    abort(); \
} while (0)

#ifdef DEBUG_ON
#define DEBUG(fmt, ...) do { \
    fprintf(stdout, "[%d]:[%x]: ", thread_id, exec_curr_insn); \
    fprintf(stdout, fmt, ##__VA_ARGS__); \
} while (0)
#else
#define DEBUG(...) do {} while (0)
#endif

#ifdef INFO_ON
#define INFO(fmt, ...) do { \
    fprintf(stdout, "[%d]:[%x]: ", thread_id, exec_curr_insn); \
    fprintf(stdout, fmt, ##__VA_ARGS__); \
} while (0)
#else
#define INFO(...) do {} while (0)
#endif

#endif
