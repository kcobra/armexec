cmake_minimum_required(VERSION 2.8)

project (armexec)

set(CMAKE_C_FLAGS "-ggdb -O0 -Wall -Werror -Wno-unused")

add_executable(armexec vm.c elf.c exec.c plt_stub.c string_stub.c socket_stub.c
time_stub.c pthread_stub.c sprintf_stub.c stdlib_stub.c mq_stub.c libc_obj.c
main.c)

target_link_libraries(armexec capstone pthread rt)
